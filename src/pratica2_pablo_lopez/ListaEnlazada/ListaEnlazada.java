/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pratica2_pablo_lopez.ListaEnlazada;

import pratica2_pablo_lopez.MemoryUtil;

/**
 *
 * @author Usuario
 */
public class ListaEnlazada {
    
    private Nodo primero;
    private int size;
    
    public ListaEnlazada(){
        this.primero=null;
        this.size =0;
    }
     
    public void ListaVacia(){
        if(primero == null){
            System.out.println("La lista esta vacia ");
        }else{
            System.out.println("la lista esta llena ");
        }
    }
    
    public void addNodo(int dato){
        Nodo nuevo = new Nodo(dato);
        nuevo.siguiente=primero;
        primero=nuevo;
        size++;
    }
    
    public int Size (){
        return size;
    }
    
    public void Listar(){
        Nodo actual = primero;
        while(actual!=null){
            System.out.println("("+actual.dato+")");
            actual=actual.siguiente;
        }
    }
    
    public static void main (String [] args ){
        ListaEnlazada lista=new ListaEnlazada();
        System.out.println("Lista con datos: ");
        System.out.println("");
        
        lista.addNodo(3);
        lista.addNodo(2);
        lista.addNodo(1);
        lista.Listar();
        
        System.out.println("Tamaño:  " + lista.size);
        System.out.println("");
        lista.ListaVacia();
        
        System.out.println("");
        System.out.println("El tamaño de la lista es: ");
        System.out.println(MemoryUtil.deepSizeOf(lista));
        
        
    } 
}
