/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pratica2_pablo_lopez;

import java.util.ArrayList;
import org.github.jamm.MemoryMeter;

public class Arreglos {
   public static void main(String[] args){
        int numero= 3;
        long largo = 56;
        byte bit = 45;
        char chair = 12;
        boolean A=true;
    
        
        ArrayList <String> list = new ArrayList();
        System.out.println(MemoryUtil.deepSizeOf(numero));
        list.add("Hools");
        System.out.println(MemoryUtil.deepSizeOf(list));
        
        ArrayList <Double> aux = new ArrayList();
        System.out.println(MemoryUtil.deepSizeOf(largo));
        aux.add(2.5);
        System.out.println(MemoryUtil.deepSizeOf(aux));
        
        ArrayList <Float> a = new ArrayList();
        System.out.println(MemoryUtil.deepSizeOf(bit));
        a.add(Float.NaN);
        System.out.println(MemoryUtil.deepSizeOf(a));
        
        MemoryMeter e= MemoryMeter.builder().build();
        System.out.println(e.measureDeep(chair));
        System.out.println(MemoryUtil.deepSizeOf(chair));
        
        ArrayList <Boolean> memoria = new ArrayList();
        System.out.println(MemoryUtil.deepSizeOf(numero));
        memoria.add(A);
        System.out.println(MemoryUtil.shallowSizeOf(memoria));
//        
   }
}
